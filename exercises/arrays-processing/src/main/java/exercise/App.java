package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] getElementsLessAverage(int[] numbers) {
        double averageValue;
        double tempValue;
        int endPosition = 0;

        if (numbers.length == 0) {
            return new int[numbers.length];
        }

        averageValue = getAverageValue(numbers);
        Arrays.sort(numbers);

        for (int i = 0; i < numbers.length - 1; i++) {

            tempValue = (double) numbers[i];
            if (tempValue >= averageValue) {
                endPosition = i;
                break;
            }
        }

        return Arrays.copyOfRange(numbers, 0, endPosition);
    }

    public static double getAverageValue(int[] numbers) {
        double sum = 0;

        for (double tempValue: numbers) {
            sum += tempValue;
        }

        return sum / numbers.length;
    }

    public static int getSumBeforeMinAndMax(int[] numbers) {
        if (numbers.length == 0) {
            return 0;
        }

        int sum = 0;
        int[] sortNumbers = numbers.clone();
        int minValue;
        int maxValue;
        boolean isSumming = false;

        Arrays.sort(sortNumbers);
        minValue = sortNumbers[0];
        maxValue = sortNumbers[sortNumbers.length - 1];

        for (int i: numbers) {
            if (!isSumming && (i == minValue || i == maxValue)) {
                isSumming = true;
            } else if (isSumming && (i == minValue || i == maxValue)) {
                isSumming = false;
            } else if (isSumming) {
                sum += i;
            }
        }

        return sum;
    }
    // END
}
