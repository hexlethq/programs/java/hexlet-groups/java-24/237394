package exercise;

class Converter {
    // BEGIN
    public static int convert(int sizeOfFile, String direction) {
        if (direction == "b") {
            return sizeOfFile * 1024;
        } else if (direction == "kb") {
            return (int) sizeOfFile / 1024;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
