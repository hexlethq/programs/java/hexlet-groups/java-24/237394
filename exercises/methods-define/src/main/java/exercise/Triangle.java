package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int angle) {
        return 0.5 * a * b * Math.sin(angle * Math.PI / 180);
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
