package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {

        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            if (a == b && a == c) {
                return "Равносторонний";
            } else if (a == b || a == c || b == c) {
                return "Равнобедренный";
            } else {
                return "Разносторонний";
            }
        } else {
            return "Треугольник не существует";
        }
    }
    // END
}
