package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String longPhrase) {
        String resultString = "";
        String[] words = longPhrase.split(" ");
        for (String word: words) {
            Character firstChar = word.charAt(0);
            String firstSymbol = firstChar.toString();
            resultString = resultString + firstSymbol.toUpperCase();
        }
        return resultString;
    }
    // END
}
