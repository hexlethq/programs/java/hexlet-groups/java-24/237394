package exercise;

class Point {
    // BEGIN
    private int x;
    private int y;

    Point(int x0, int y0) {
        this.x = x0;
        this.y = y0;
    }

    public static Point makePoint(int x, int y) {
        return new Point(x, y);
    }

    public static int getX(Point point) {
        return point.x;
    }

    public static int getY(Point point) {
        return point.y;
    }

    public static String pointToString(Point point) {
        return "(" + Point.getX(point) + ", " + Point.getY(point) + ")";
    }

    public static int getQuadrant(Point point) {
        int x = Point.getX(point);
        int y = Point.getY(point);

        if (x == 0 || y == 0) {
            return 0;
        }

        if (x > 0 && y > 0) {
            return 1;
        } else if (x < 0 && y > 0) {
            return 2;
        } else if (x > 0 && y < 0) {
            return 4;
        } else {
            return 3;
        }
    }

    public static Point getSymmetricalPointByX(Point point) {
        return makePoint(Point.getX(point), -Point.getY(point));
    }

    public static double calculateDistance(Point point1, Point point2) {
        double x1 = (double) Point.getX(point1);
        double y1 = (double) Point.getY(point1);
        double x2 = (double) Point.getX(point2);
        double y2 = (double) Point.getY(point2);

        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }
    // END
}
