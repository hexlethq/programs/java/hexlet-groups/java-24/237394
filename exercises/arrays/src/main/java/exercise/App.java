package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] oldArray) {
        int[] newArray = new int[oldArray.length];

        for (int i = 0; i < oldArray.length; i++) {
            newArray[i] = oldArray[oldArray.length - i - 1];
        }

        return newArray;
    }

    public static int getIndexOfMaxNegative(int[] arrayOfIntegers) {
        if (arrayOfIntegers.length == 0) {
            return -1;
        }

        int result = -1;
        int maxNegativeValue = 0;

        for (int i = 0; i < arrayOfIntegers.length - 1; i++) {
            int elementOfArray = arrayOfIntegers[i];
            if (maxNegativeValue == 0) {
                if (elementOfArray < 0) {
                    result = i;
                    maxNegativeValue = elementOfArray;
                }
            } else if (maxNegativeValue < 0) {
                if (elementOfArray < 0 && elementOfArray > maxNegativeValue) {
                    result = i;
                    maxNegativeValue = elementOfArray;
                }
            }
        }

        return result;
    }
    // END
}
