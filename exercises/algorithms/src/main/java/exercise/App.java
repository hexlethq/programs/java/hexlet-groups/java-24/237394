package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] incomingArray) {
        if (incomingArray.length == 0) {
            return new int[incomingArray.length];
        }

        for (int i = 0; i < incomingArray.length; i++) {
            for (int j = incomingArray.length - 1; j > i; j--) {
                if (incomingArray[j] < incomingArray[j - 1]) {
                    incomingArray[j] = incomingArray[j] + incomingArray[j - 1];
                    incomingArray[j - 1] = incomingArray[j] - incomingArray[j - 1];
                    incomingArray[j] = incomingArray[j] - incomingArray[j - 1];
                }
            }
        }

        return incomingArray;
    }
    // END
}
