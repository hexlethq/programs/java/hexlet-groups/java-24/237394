package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class App {
    // BEGIN
    public static String buildList(String[] animals) {
        if (animals.length == 0) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<ul>\n");
        for (String animal: animals) {
            builder.append("  <li>" + animal + "</li>\n");
        }
        builder.append("</ul>");

        return builder.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        if (users.length == 0) {
            return "";
        }

        String result;
        LocalDate localDate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Integer tempYear;

        StringBuilder builder = new StringBuilder();
        builder.append("<ul>\n");
        for (int i = 0; i < users.length; i++) {
            localDate = LocalDate.parse(users[i][1], formatter);
            tempYear = localDate.getYear();
            if (tempYear.equals(year)) {
                builder.append("  <li>" + users[i][0] + "</li>\n");
            }
        }

        result = builder.toString();
        if (result.equals("<ul>\n")) {
            return "";
        }

        builder.append("</ul>");

        return builder.toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        if (users.length == 0) {
            return "";
        }

        return "";
        // END
    }
}
